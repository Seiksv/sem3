
import UIKit
import Foundation

enum TextSymbols {
    case squiggle
    case circle
    case diamond
}
enum TextColors {
    case red
    case purple
    case green
}
enum TextStyles {
    case filled
    case nonFilled
    case striped
}

class MyCustomView: UIView {
    var color: TextColors = .red
    var symbol: TextSymbols = .diamond
    var style: TextStyles = .filled
    var path: UIBezierPath!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init (frame: CGRect, color: TextColors, symbol: TextSymbols, style: TextStyles) {
        super.init(frame: frame)
        self.color = color
        self.symbol = symbol
        self.style = style
    }
    
    override func draw(_ rect: CGRect) {
        switch self.symbol {
            case .squiggle:
                createSquiggle()
            case .circle:
                creatOval()
            case .diamond:
                createDiamond()
        }
        
        switch self.color {
            case .red:
                UIColor.red.setFill()
                UIColor.red.setStroke()
            case .purple:
                UIColor.purple.setFill()
                UIColor.purple.setStroke()
            case .green:
                UIColor.green.setFill()
                UIColor.green.setStroke()
        }
        
        switch style {
            case .nonFilled:
                path.fill(with: CGBlendMode.multiply, alpha: 0.3)
            case .filled:
                path.fill()
            case .striped:
                fillWithStripes()
        }
        
        path.lineWidth = 3.0
        path.stroke()
        self.layer.backgroundColor = UIColor.white.cgColor
        self.backgroundColor = UIColor.white
    }

    private func creatOval () {
        path = UIBezierPath(ovalIn: CGRect(x: self.frame.midX/2,
                                           y: self.frame.size.height/5,
                                           width: self.frame.size.width/2,
                                           height: self.frame.size.height/1.5))
        path.close()
    }
    
    private func createSquiggle() {
        path = UIBezierPath()
     
        path.move(to: CGPoint(x: 104.0, y: 15.0))
       
        path.addCurve(to: CGPoint(x: 63.0, y: 54.0), controlPoint1: CGPoint(x: 112.4, y: 36.9), controlPoint2: CGPoint(x: 89.7, y: 60.8))
        path.addCurve(to: CGPoint(x: 27.0, y: 53.0), controlPoint1: CGPoint(x: 52.3, y: 51.3), controlPoint2: CGPoint(x: 42.2, y: 42.0))
        path.addCurve(to: CGPoint(x: 5.0, y: 40.0), controlPoint1: CGPoint(x: 9.6, y: 65.6), controlPoint2: CGPoint(x: 5.4, y: 58.3))
        path.addCurve(to: CGPoint(x: 36.0, y: 12.0), controlPoint1: CGPoint(x: 4.6, y: 22.0), controlPoint2: CGPoint(x: 19.1, y: 9.7))
        path.addCurve(to: CGPoint(x: 89.0, y: 14.0), controlPoint1: CGPoint(x: 59.2, y: 15.2), controlPoint2: CGPoint(x: 61.9, y: 31.5))
        path.addCurve(to: CGPoint(x: 104.0, y: 15.0), controlPoint1: CGPoint(x: 95.3, y: 10.0), controlPoint2: CGPoint(x: 100.9, y: 6.9))

  
        path.close()
    }

    private func createDiamond() {
        path = UIBezierPath()
        path.move(to: CGPoint(x: self.frame.width/2, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: self.frame.size.height/2))
        path.addLine(to: CGPoint(x: self.frame.width/2, y: self.frame.height))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height/2))
        path.close()
    }
    
    private func fillWithStripes() {
        let stripe = UIBezierPath()
        stripe.move(to: CGPoint(x: path.cgPath.boundingBox.minX, y: path.cgPath.boundingBox.minY))
        stripe.addLine(to: CGPoint(x: path.cgPath.boundingBox.minX, y: path.cgPath.boundingBox.maxY))
        stripe.lineWidth = 1.0
        path.addClip()
        
        for _ in 1...Int(path.cgPath.boundingBox.maxX / 5) {
            stripe.apply(CGAffineTransform(translationX: 5, y: 0))
            stripe.stroke()
        }
        path.close()
    }
}
