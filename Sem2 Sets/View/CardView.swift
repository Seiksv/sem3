//
//  CardView.swift
//  Sem2 Sets
//
//  Created by APPLAUDO on 16/2/22.
//

import UIKit

class CardView: UIView {
    var viewController = ViewController()

    override init(frame: CGRect) {
          super.init(frame: frame)
       
          self.backgroundColor = UIColor.darkGray
      }
       
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
      }

    override func draw(_ rect: CGRect) {
        viewController.bounds=self.bounds
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
            self.setNeedsDisplay()
            self.setNeedsLayout()
        }


}
