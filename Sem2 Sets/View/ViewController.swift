//
//  ViewController.swift
//  Sem2 Sets
//
//  Created by APPLAUDO on 9/2/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var buttonAddCards: UIButton!
    @IBOutlet weak var labelCheat: UILabel!
    @IBOutlet weak var labelCardLefts: UILabel!
    @IBOutlet weak var labelFeedback: UILabel!
    @IBOutlet weak var labelTiming: UILabel!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var labelIphoneScore: UILabel!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var labelIphone: UILabel!
    
    @IBAction func swipeDown(_ sender: UISwipeGestureRecognizer) {
        removeAndAddCards(numberOfCads: 2)
    }
    @IBAction func randomizeCards(_ sender: UIRotationGestureRecognizer) {
        currentCards.shuffle()
        setupVIew()
    }
    @IBAction func newGame(_ sender: UIButton) {
        newGame()
    }
    
    @IBAction func newGameWithIphone(_ sender: UIButton) {
        newGame()
    }
    
    @IBAction func buttonAddCards(_ sender: UIButton) {
        removeAndAddCards(numberOfCads: 2)
    }
    
    var buttonCards: [UIView] = []
    var buttonSelectedCards: [Int:UIView] = [:]
    var bounds: CGRect = CGRect()
    var possibleSimbols = [Int:String]()
    var setController = SetController()
    var cellCount = 12
    var cardsInDeck = [Card]()
    var currentCards = [Card]()
    var currentSelectedCards: [Int] = []
    private lazy var grid = Grid(layout: .aspectRatio(0.5))
    
    override func viewDidLoad() {
        super.viewDidLoad()
         cardsInDeck = setController.generateAllRamdomCards(numberOfCards: 81)
        removeAndAddCards(numberOfCads: cellCount-1)
        setupVIew()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupVIew()
    }
    
    private func newGame(){
         buttonCards = []
         buttonSelectedCards = [:]
         bounds = CGRect()
         possibleSimbols = [Int:String]()
         cellCount = 12
         grid = Grid(layout: .aspectRatio(0.5))
         cardsInDeck = [Card]()
         currentCards = [Card]()
         currentSelectedCards = []
        setController.newGame()
        cardsInDeck = setController.generateAllRamdomCards(numberOfCards: 81)
        removeAndAddCards(numberOfCads: cellCount-1)
        setupVIew()
    }
    
    private func removeAndAddCards(numberOfCads: Int){
        if !cardsInDeck.isEmpty && cardsInDeck.count >= 3{
            currentCards += Array(cardsInDeck[0...numberOfCads])
            cardsInDeck.removeSubrange(0...numberOfCads)
            cellCount = currentCards.count
            setupVIew()
        } else {
            buttonAddCards.isEnabled = false
        }
    }
    
    private func setupVIew(){
        labelScore.text = "\(setController.globalScore)"
        containerUIView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        grid = Grid(layout: .aspectRatio(0.5), frame: containerUIView.bounds)
        grid.cellCount = cellCount
        buttonCards.removeAll()
        for index in 0..<cellCount {
            let view = UIView()
            var gesture = UITapGestureRecognizer()
            gesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            gesture.numberOfTapsRequired = 1
            gesture.numberOfTouchesRequired = 1
            view.isUserInteractionEnabled = true
            view.addGestureRecognizer(gesture)
            view.backgroundColor = .orange
            view.layer.borderColor = UIColor.white.cgColor
            view.layer.borderWidth = 2.0
            view.frame = grid[index] ?? .zero
            view.translatesAutoresizingMaskIntoConstraints = true
            
            
            let stackView = getStackView(view: view, card: currentCards[index])
            view.addSubview(stackView)
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: CGFloat(0.0)).isActive = true
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: CGFloat(0.0)).isActive = true
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -CGFloat(0.0)).isActive = true
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -CGFloat(0.0)).isActive = true
            
            buttonCards.append(view)
            containerUIView.addSubview(view)
        }
    }
    
    private func cardsMatchUp() {
        
        var countOfCommonsAttributes = 0
        if currentCards[currentSelectedCards[0]].textSyle ==  currentCards[currentSelectedCards[1]].textSyle && currentCards[currentSelectedCards[1]].textSyle == currentCards[currentSelectedCards[2]].textSyle {
            countOfCommonsAttributes += 1
        }
        
        if currentCards[currentSelectedCards[0]].simbol ==  currentCards[currentSelectedCards[1]].simbol && currentCards[currentSelectedCards[1]].simbol == currentCards[currentSelectedCards[2]].simbol {
            countOfCommonsAttributes += 1
        }
        
        if currentCards[currentSelectedCards[0]].color ==  currentCards[currentSelectedCards[1]].color && currentCards[currentSelectedCards[1]].color == currentCards[currentSelectedCards[2]].color {
            countOfCommonsAttributes += 1
        }
        
        if currentCards[currentSelectedCards[0]].numberOfSimbolsByCard ==  currentCards[currentSelectedCards[1]].numberOfSimbolsByCard && currentCards[currentSelectedCards[1]].numberOfSimbolsByCard == currentCards[currentSelectedCards[2]].numberOfSimbolsByCard {
            countOfCommonsAttributes += 1
        }
        
        if (countOfCommonsAttributes > 2 ){
            
            buttonSelectedCards.forEach{ (card) in
                card.value.layer.borderColor = UIColor.white.cgColor
            }
            currentCards.removeIndices(elements: currentSelectedCards)
            cellCount -= 3
            currentSelectedCards.removeAll()
            setController.setScore(value: .match)
            setupVIew()
        } else {
            currentSelectedCards.removeAll()
            setController.setScore(value: .mismatch)
            buttonSelectedCards.forEach{ (card) in
                card.value.layer.borderColor = UIColor.white.cgColor
            }
            setupVIew()
        }
    }
    
    private func getStackView(view: UIView, card: Card) -> UIStackView{
        let stackView = UIStackView()
        let number = card.numberOfSimbolsByCard + 1
        
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = UIStackView.Distribution.fillProportionally
        stackView.alignment = UIStackView.Alignment.fill
        for _ in 0..<number {
            let customView3 = MyCustomView(frame: view.frame,
                                           color: card.color,
                                           symbol: card.simbol,
                                           style: card.textSyle)
                        
            customView3.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
            customView3.heightAnchor.constraint(equalToConstant: view.frame.height/CGFloat(number)).isActive = true
            customView3.backgroundColor = .clear
            stackView.addArrangedSubview(customView3)
        }
        return stackView
    }
    
    @objc func handleTap(_ sender : UITapGestureRecognizer? = nil) {
       
        if let senderView = sender , let view = senderView.view {
            if let index = buttonCards.firstIndex(of: view){
                
                if currentSelectedCards.contains(index) {
                    setController.setScore(value: .flipped)
                    view.layer.borderColor = UIColor.white.cgColor
                    view.layer.borderWidth = 2.0
                    currentSelectedCards.remove(at:  currentSelectedCards.firstIndex(of: index)!)
                    buttonSelectedCards[index] = nil
                    return
                } else {
                    view.layer.borderColor = UIColor.black.cgColor
                    view.layer.borderWidth = 2.0}
                    buttonSelectedCards[index] = view
                    currentSelectedCards.append(index)
                    if currentSelectedCards.count == 3{
                        cardsMatchUp()
                    }
            }
        
        }
    }

    
}

