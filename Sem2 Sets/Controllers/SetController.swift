//
//  SetController.swift
//  Sem2 Sets
//
//  Created by APPLAUDO on 10/2/22.
//

import Foundation
import UIKit


class SetController {
    
    enum ScoreFeedback {
        case flipped
        case mismatch
        case match
        case withAnScreenSolution
    }
    
    var possiblesCardsValues = [
        TextSymbols.squiggle,
        TextSymbols.circle,
        TextSymbols.diamond
    ]
    
    var possiblesColors = [
        TextColors.red,
        TextColors.purple,
        TextColors.green,
    ]
    
    var possiblesTextStyles = [
        TextStyles.filled,
        TextStyles.nonFilled,
        TextStyles.striped
    ]
    
    var outputMessage = ""
    var globalScore = 0
    var visibleCardsCount = 12
    var listOfCombinations: [Card] = []
    let possibleSimbolsByCard = 3
    
    func showExpectedCards(buttonCards: [UIView]) {
        for cardIndice in buttonCards.indices{
            if cardIndice < visibleCardsCount {
                buttonCards[cardIndice].isHidden = false
            } else {
                buttonCards[cardIndice].isHidden = true
            }
        }
        visibleCardsCount += 3
    }
    
    func generateCardCombinations () -> Card {
           let randomSimbol = possiblesCardsValues.count.randomNumber
           let randomNumberOfSymbols = possibleSimbolsByCard.randomNumber
           let randomPossibleColor = possiblesColors.count.randomNumber
           let randomTextStyle = possiblesTextStyles.count.randomNumber
           let card = Card(
               color: Array(possiblesColors)[randomPossibleColor],
               simbol: Array(possiblesCardsValues)[randomSimbol],
               textSyle: Array(possiblesTextStyles)[randomTextStyle],
               numberOfSimbolsByCard: randomNumberOfSymbols)
        
           return card;
       }
       
       func generateAllRamdomCards (numberOfCards: Int) -> [Card] {
           for _ in 0 ... numberOfCards {
               listOfCombinations.append(generateCardCombinations())
           }
           return listOfCombinations
       }

    func setScore(value: ScoreFeedback){
        switch value {
            case .flipped:
                outputMessage = "Hmmmmm dont look back, -1 points!"
                globalScore += -1
                break
            case .mismatch:
                outputMessage = "You missed, -5 points!"
                globalScore += -5
                break
            case .match:
                outputMessage = "Good job, +3 points!"
                globalScore += 3
                break
            case .withAnScreenSolution:
                outputMessage = "You miss a solution in screen, -1 Points!"
                globalScore += -1
                break
        }
    }
    
    func newGame(){
        visibleCardsCount = 12
        listOfCombinations = []
    }
}

extension Int {
    var randomNumber: Int {
        return Int(arc4random_uniform(UInt32(self)))
    }
}

extension Array {
    mutating func removeIndices(elements indicesToRemove: [Int]) {
        _ = indicesToRemove.map { self[$0] }
        for indexToRemove in indicesToRemove.sorted(by: >) {
            remove(at: indexToRemove)
        }
    }
}
